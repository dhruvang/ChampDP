(function ($) {
        $.fn.ChampDP = function (options) {
            var settings = $.extend({
                container: $(this),
            }, options);
            var selector = settings.container;
            $(selector).find('select').hide();
            $(selector).find('select').before('<ul class="options"></ul>');
            $(selector).find('select').find('option').each(function (i, el) {
                var option = $(this).text();
                var value = $(this).val();
                $(selector).find(".options").append('<li data-value="'+value+'">' + option + '</li>');    
            });
            var title = $(selector).find(".options").find('li').first().text();
            $(selector).find(".options").before('<p class="open_option">' + title + '</p>');
            $(selector).find(".options").hide();
            if($(selector).find(".options:disabled").length > 0){
                $(selector).find(".options").find('li').first().addClass('disabled')        
            }
            $(selector).find(".options").find('li').first().addClass('selected')            
            $(selector).find(".open_option").click(function (e) {
                e.stopPropagation();
                $(selector).not(this).find(".options").slideToggle('fast');
    
            });
    
            $(selector).find(".options li:not(.disabled)").click(function () {
                var selectedVal = $(this).html();
                var selectValue = $(this).data("value");
                $(selector).find(".open_option").text(selectedVal);
                $(selector).find('select').val(selectValue).change();
                $(selector).find(".options").slideUp('fast');
            });
        }
    }(jQuery));
